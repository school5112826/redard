import io.github.bonigarcia.wdm.WebDriverManager;
import io.github.bonigarcia.wdm.config.DriverManagerType;
import org.example.pages.*;

import org.junit.Test;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.Assert;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.openqa.selenium.io.FileHandler;

public class RegardTest extends BasePage {
    @Test
    public void TestScenario() {

        MainPage mainPage = new MainPage();
        CatalogSection catalogSection = new CatalogSection();
        MotherboardsPage motherboardsPage = new MotherboardsPage();
        CasesPage casesPage = new CasesPage();
        CartPage cartPage = new CartPage();

        mainPage.open();
        mainPage.clickCatalogButton();

        catalogSection.selectMotherboards();
        motherboardsPage.addMotherboardToCart();

        mainPage.clickCatalogButton();

        catalogSection.selectCases();
        casesPage.filterByManufacturer();
        casesPage.addCaseToCart();

        cartPage.goToCart();

        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }

        Assert.assertTrue(cartPage.getCountOfCartElements() >= 2,
                "Кол-во эл-ов в корзине меньше 2х");
        System.out.println("Элементов в корзине: " + cartPage.getCountOfCartElements());
    }

    @Test
    public void takeScreenshot() {
        WebDriverManager.getInstance(DriverManagerType.CHROME).setup();
        ChromeDriver driver = new ChromeDriver();

        driver.get("https://www.regard.ru/");
        File source = ((RemoteWebDriver)driver).getScreenshotAs(OutputType.FILE);
        try {
            String fileSuffix = new SimpleDateFormat("yyyyMMdd-HHmmss").format(new Date());
            FileHandler.copy(source, new File(fileSuffix + "-scr.png"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}

