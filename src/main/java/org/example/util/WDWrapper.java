package org.example.util;

import io.github.bonigarcia.wdm.WebDriverManager;
import io.github.bonigarcia.wdm.config.DriverManagerType;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.example.pages.BasePage;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.io.FileHandler;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Set;

public class WDWrapper implements WebDriver {

    private static WebDriver driver;
    public static Logger log = LogManager.getLogger(BasePage.class);

    private static WDWrapper wd;

    public static WDWrapper getInstance() {
        if (wd == null) {
            wd = new WDWrapper();
        }
        return wd;
    }

    private WDWrapper() {
    }

    static {
        WebDriverManager.getInstance(DriverManagerType.CHROME).setup();

        ChromeOptions options = new ChromeOptions();
        options.setExperimentalOption("excludeSwitches", Collections.singletonList("disable-automation"));

        driver = new ChromeDriver();
        driver.manage().window().maximize();
    }

    public void waitLoad() {
        int steps = 3;
        for (int i = 1; i < steps; i++) {
            try {
                WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(5));
                wait.until(ExpectedConditions.visibilityOfAllElements());
            } catch (TimeoutException e) {
                continue;
            }
        }
    }

    public static void highlightElementsByJS(String color, WebElement element) {
        ((JavascriptExecutor)driver).executeScript(
                String.format("arguments[0].style.backgroundColor='%s'", color),
                element
        );
    }

    public void takeScreenshot(){
        File source = ((RemoteWebDriver)driver).getScreenshotAs(OutputType.FILE);
        try {
            String fileSuffix = new SimpleDateFormat("yyyyMMdd-HHmmss").format(new Date());
            FileHandler.copy(source, new File(fileSuffix + "-scr.png"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void get(String url) {
        log.debug("Открываю страницу с сайтом");
        driver.get("https://www.regard.ru/");
    }

    @Override
    public String getCurrentUrl() {
        return null;
    }

    @Override
    public String getTitle() {
        return null;
    }

    @Override
    public List<WebElement> findElements(By by) {
        List<WebElement> elements = null;
        try {
            log.debug("Ищу элемент по локатору: {} ", by);
            elements = driver.findElements(by);
            log.debug("Нашел элементы в корзине по локатору: {} ", by);
        } catch (TimeoutException e) {
            waitLoad();
            takeScreenshot();
        }
        return elements;
    }

    @Override
    public WebElement findElement(By by) {
        WebElement element;
        try {
            log.debug("Ищу элемент по локатору: {} ", by);
            element = driver.findElement(by);
            highlightElementsByJS("rgb(0,0,250)",element);
            log.debug("Нашел элементы в корзине по локатору: {} ", by);
        } catch (NoSuchElementException | StaleElementReferenceException | ElementClickInterceptedException e) {
            waitLoad();
            takeScreenshot();
            throw e;
        }
        return element;
    }


    @Override
    public String getPageSource() {
        return driver.getPageSource();
    }

    @Override
    public void close() {

    }

    @Override
    public void quit() {
        driver.quit();
    }

    @Override
    public Set<String> getWindowHandles() {
        return Collections.singleton(driver.getPageSource());
    }

    @Override
    public String getWindowHandle() {
        return driver.getPageSource();
    }

    @Override
    public TargetLocator switchTo() {
        return null;
    }

    @Override
    public Navigation navigate() {
        return null;
    }

    @Override
    public Options manage() {
        return null;
    }
}
