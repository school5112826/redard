package org.example.pages;

import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;

public class CasesPage extends BasePage {

    public void filterByManufacturer() {
        try {
            Thread.sleep(3000); // Подождать 5 секунд
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }

        WebElement manufacturerCheckbox = driver.findElement(By.xpath("(//label[@for='id-AeroCool-AeroCool'])[1]"));
        manufacturerCheckbox.click();
    }

    public void addCaseToCart() {
        int steps = 5;
        for (int i = 1; i < steps; i++) {
            try {
                WebElement addToCartButton = driver.findElement(By.xpath("(//button[@type='button'])[6]"));
                WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(3));
                wait.until(ExpectedConditions.visibilityOf(addToCartButton));
                addToCartButton.click();
                return;
            } catch (StaleElementReferenceException | ElementClickInterceptedException e) {
                continue;
            }
        }

    }
}