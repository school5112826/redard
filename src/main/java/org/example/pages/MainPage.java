package org.example.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

public class MainPage extends BasePage {

    public void clickCatalogButton() {
        WebElement catalogButton = driver.findElement(By.xpath("(//button[@type='button'])[1]"));
        catalogButton.click();
    }
}
