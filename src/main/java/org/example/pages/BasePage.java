package org.example.pages;

import org.example.util.WDWrapper;
import org.openqa.selenium.WebDriver;

public class BasePage {

    public static WebDriver driver = WDWrapper.getInstance();

    public void open() {
        driver.get("https://www.regard.ru/");
    }

}
