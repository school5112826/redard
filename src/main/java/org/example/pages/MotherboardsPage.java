package org.example.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.ElementClickInterceptedException;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;

public class MotherboardsPage extends BasePage {
    public void addMotherboardToCart() {
    int steps = 5;
       for (int i = 1; i < steps; i++) {
            try {
                WebElement addToCartButton = driver.findElement(By.xpath("(//button[@type='button'])[10]"));
                WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(5));
                wait.until(ExpectedConditions.visibilityOf(addToCartButton));
                addToCartButton.click();
                return;
            } catch (StaleElementReferenceException | ElementClickInterceptedException e) {
                continue;
            }
        }
    }
}