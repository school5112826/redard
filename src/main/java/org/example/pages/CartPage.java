package org.example.pages;

import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;
import java.util.List;

public class CartPage extends BasePage {

    String elementsInCart = "//div[contains(@class, 'BasketItem_wrap')]";

    public void goToCart() {
                WebElement goToCart = driver.findElement(By.xpath("(//a[@href='/cart'])"));
                goToCart.click();
    }


    public int getCountOfCartElements() {
        List<WebElement> items = driver.findElements(By.xpath(elementsInCart));
        return items.size();
    }
}
