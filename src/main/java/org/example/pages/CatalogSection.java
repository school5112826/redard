package org.example.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

public class CatalogSection extends BasePage{

    public void selectMotherboards() {
        WebElement lga1200Link = driver.findElement(By.xpath("(//a[@href='/reciept/119714/materinskie-platy-lga-1200'])[1]"));
        lga1200Link.click();
    }

    public void selectCases() {
        WebElement atxLink = driver.findElement(By.xpath("(//a[@href='/reciept/24065/korpusa-atx'])[1]"));
        atxLink.click();
    }
}